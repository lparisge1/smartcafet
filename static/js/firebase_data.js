var firebaseConfig = {
  apiKey: "AIzaSyA2jGTduk_kR4duJNBmHMSHj27KBU3BWjA",
  authDomain: "smartcafetinsa.firebaseapp.com",
  databaseURL: "https://smartcafetinsa-default-rtdb.firebaseio.com",
  projectId: "smartcafetinsa",
  storageBucket: "smartcafetinsa.appspot.com",
  messagingSenderId: "55571023912",
  appId: "1:55571023912:web:01569ee640067b33b1cd1d",
  measurementId: "G-P3JHCGB4HK"
};

//Fonctions d'Hugo
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var coca = document.getElementById('coca');
var dbRefCoca = firebase.database().ref().child('Coca');
dbRefCoca.on('value', snap => coca.innerText = snap.val());

var eau = document.getElementById('eau');
var dbRefEau = firebase.database().ref().child('Eau');
dbRefEau.on('value', snap => eau.innerText = snap.val());

var frites = document.getElementById('frites');
var dbRefFrites = firebase.database().ref().child('Frites');
dbRefFrites.on('value', snap => frites.innerText = snap.val());

var fruits = document.getElementById('fruits');
var dbRefFruits = firebase.database().ref().child('Fruits');
dbRefFruits.on('value', snap => fruits.innerText = snap.val());

var hamburger = document.getElementById('hamburger');
var dbRefHamburger = firebase.database().ref().child('Hamburger');
dbRefHamburger.on('value', snap => hamburger.innerText = snap.val());

var oasis = document.getElementById('oasis');
var dbRefOasis = firebase.database().ref().child('Oasis');
dbRefOasis.on('value', snap => oasis.innerText = snap.val());

var panini = document.getElementById('panini');
var dbRefPanini = firebase.database().ref().child('Panini');
dbRefPanini.on('value', snap => panini.innerText = snap.val());

var sandpoul = document.getElementById('sandwich1');
var dbRefSandPou = firebase.database().ref().child('Sandwich poulet');
dbRefSandPou.on('value', snap => sandpoul.innerText = snap.val());

var sandcru = document.getElementById('sandwich2');
var dbRefSandCru = firebase.database().ref().child('Sandwich crudités');
dbRefSandCru.on('value', snap => sandcru.innerText = snap.val());

var yaourt = document.getElementById('yaourt');
var dbRefYaourt = firebase.database().ref().child('Yaourt');
dbRefYaourt.on('value', snap => yaourt.innerText = snap.val());

var crudites = document.getElementById('crudites');
var dbRefCrudites = firebase.database().ref().child('Crudités');
dbRefCrudites.on('value', snap => crudites.innerText = snap.val());
//Fin fonctions Hugo



//Fonction Ludovic
function resetDB(){ //Set all data in firebase database to 0
  dbRefEau.set(0);
  dbRefFrites.set(0);
  dbRefFruits.set(0);
  dbRefHamburger.set(0);
  dbRefOasis.set(0);
  dbRefPanini.set(0);
  dbRefSandCru.set(0);
  dbRefSandPou.set(0);
  dbRefYaourt.set(0);
  dbRefCoca.set(0);
  dbRefCrudites.set(0);
}