Proof of concept developped by Ludovic Paris using open-source software and a Raspberry Pi
Contact me : lparis@itii-normandie.fr

Python file :
Run a developpment server at 127.0.0.1:5000
Do not need any change when updating database, thunkable, javascript or html file !

HTML file :
Architecture of the web site. Bootstrap is use to make it pretty.
In case you want to update the database :
    - update the table
    - Look for the tag "table" in the HTML file
    - Add/Delete the row you need.
    - Documentation to modify the table : https://getbootstrap.com/docs/4.0/content/tables/

Javascript file :
Use to connect to the database, get value and store then in variable, set value to 0.
In case you want to update the database :
    - modify this according to your database key and HTML variable :
        var your_var = document.getElementById('your_var');
        var your_db_var = firebase.database().ref().child('your_firebase_object_key');
        your_db_var.on('value', snap => your_var.innerText = snap.val());

Thunkable :
Use to create a simple phone application.
In case you want to update the database:
    - Contact me so that I can send you the original Thunkable project

Firebase :
Store data and provide a free to use web service
In case you want to update the database:
    - Contact me so that I can help you to create a firebase properly
